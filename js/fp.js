// -----------------------------
// CONSTANTES & VARIABLES GLOBALES
// -----------------------------
const MAX_CANVAS_SIZE = 2000,
    MIN_BLUR_RADIUS = 5,
    MAX_BLUR_RADIUS = 50,
    REFERENCE_FACE_WIDTH = 100;

const MAX_CANVAS_AREA = 67108864;

function getScaledDimensions(width, height) {
    let area = width * height;
    const isiOS = /iP(hone|od|ad)/.test(navigator.platform);
    let factor = 1;
    if (isiOS) {
        factor = 0.5;
    }
    width = Math.floor(width * factor);
    height = Math.floor(height * factor);
    area = width * height;

    if (area > MAX_CANVAS_AREA) {
        const scale = Math.sqrt(MAX_CANVAS_AREA / area);
        width = Math.floor(width * scale);
        height = Math.floor(height * scale);
    }
    return {
        width,
        height
    };
}

let fabricCanvas,
    faceEmojis = [],
    faceCustoms = [],
    faces = [];

let processingMode = 'blur',
    drawingMode = 'cursor',
    isDrawing = false,
    isDrawingEmoji = false,
    isModified = false,
    allSelected = false;

let originalImageData,
    originalImage,
    originalWidth,
    originalHeight,
    aspectRatio,
    scaleFactor = 1;

let customEmojiImage = null,
    customEmojiWidth = 0,
    customEmojiHeight = 0;

let brushSize,
    selectedEmoji = '😊',
    emojiSize = 30,
    faceDetections,
    uniqueDetections,
    sortedDetections;

// -----------------------------
// DOM SELECTEURS
// -----------------------------
const DOM = {
    imageInput: document.getElementById('imageInput'),
    downloadButton: document.getElementById('downloadButton'),
    selectAllButton: document.getElementById('selectAllButton'),
    widthInput: document.getElementById('widthInput'),
    heightInput: document.getElementById('heightInput'),
    sizeSlider: document.getElementById('sizeSlider'),
    sizeSliderZone: document.getElementById('sizeSlider_zone'),
    sizeSliderLabel: document.getElementById('sizeSlider_label'),
    customCursor: document.getElementById('customCursor'),
    outputCanvas: document.getElementById('outputCanvas'),
    ctx: document.getElementById('outputCanvas').getContext('2d'),
    facesContainer: document.getElementById('facesContainer'),
    loadingOverlay: document.getElementById('loadingOverlay')
};

// -----------------------------
// MODULE UTILITAIRES
// -----------------------------
const Utils = {
    getRelativePosition(event, canvas) {
        const rect = canvas.getBoundingClientRect();
        const scaleX = canvas.width / rect.width,
            scaleY = canvas.height / rect.height;
        const clientX = (event.touches && event.touches[0].clientX) || event.clientX,
            clientY = (event.touches && event.touches[0].clientY) || event.clientY;
        return {
            offsetX: (clientX - rect.left) * scaleX,
            offsetY: (clientY - rect.top) * scaleY
        };
    },
    sortDetectionsByReadingOrder(detections) {
        const items = detections.map(d => {
            const box = d.detection.box;
            return {
                detection: d,
                cx: box.x + box.width / 2,
                cy: box.y + box.height / 2,
                height: box.height
            };
        });

        items.sort((a, b) => a.cy - b.cy);

        const rows = [];
        items.forEach(item => {
            if (rows.length === 0) {
                rows.push([item]);
            } else {
                const currentRow = rows[rows.length - 1];
                const avgCy = currentRow.reduce((sum, i) => sum + i.cy, 0) / currentRow.length;
                const avgHeight = currentRow.reduce((sum, i) => sum + i.height, 0) / currentRow.length;
                if (Math.abs(item.cy - avgCy) <= avgHeight * 0.5) {
                    currentRow.push(item);
                } else {
                    rows.push([item]);
                }
            }
        });

        rows.forEach(row => row.sort((a, b) => a.cx - b.cx));

        const sortedDetections = [];
        rows.forEach(row => {
            row.forEach(item => sortedDetections.push(item.detection));
        });

        return sortedDetections;
    },
    updateDimensionProportionally(changedDimension) {
        const newWidth = parseInt(DOM.widthInput.value, 10),
            newHeight = parseInt(DOM.heightInput.value, 10);
        if (changedDimension === 'width' && !isNaN(newWidth))
            DOM.heightInput.value = Math.round(newWidth / aspectRatio);
        else if (changedDimension === 'height' && !isNaN(newHeight))
            DOM.widthInput.value = Math.round(newHeight * aspectRatio);
    },
    enlargeBox(box, scale, canvas) {
        const newWidth = box.width * scale,
            newHeight = box.height * scale * 0.8,
            newX = Math.max(0, box.x - (newWidth - box.width) / 2),
            newY = Math.max(0, box.y - (newHeight - box.height) / 2);
        return {
            x: Math.min(newX, canvas.width - newWidth),
            y: Math.min(newY, canvas.height - newHeight),
            width: Math.min(newWidth, canvas.width - newX),
            height: Math.min(newHeight, canvas.height - newY)
        };
    },
    filterDuplicateDetections(detections, iouThreshold) {
        const filtered = [];

        function computeIoU(boxA, boxB) {
            const xA = Math.max(boxA.x, boxB.x),
                yA = Math.max(boxA.y, boxB.y),
                xB = Math.min(boxA.x + boxA.width, boxB.x + boxB.width),
                yB = Math.min(boxA.y + boxA.height, boxB.y + boxB.height);
            const interArea = Math.max(0, xB - xA) * Math.max(0, yB - yA),
                boxAArea = boxA.width * boxA.height,
                boxBArea = boxB.width * boxB.height;
            return interArea / (boxAArea + boxBArea - interArea);
        }
        detections.forEach(det => {
            if (!filtered.some(fDet => computeIoU(det.detection.box, fDet.detection.box) > iouThreshold))
                filtered.push(det);
        });
        return filtered;
    }
};

// -----------------------------
// MODULE UI
// -----------------------------
const UI = {
    setActiveButton(buttonId, mode, sizeDisplay, label) {
        ['cursorButton', 'brushButton', 'eraserButton', 'emojiButton'].forEach(id => {
            document.getElementById(id).classList.toggle('active', id === buttonId);
        });
        document.getElementById('selectEmojiButton').style.display = mode === 'emoji' ? 'inline-block' : 'none';
        Drawing.setDrawingMode(mode);
        DOM.sizeSliderZone.style.display = sizeDisplay;
        if (label) DOM.sizeSliderLabel.innerText = label;
    },
    setProcessingMode(mode) {
        if (mode === 'blur' && isModified) {
            if (!confirm("Attention : les modifications effectuées avec le crayon flouteur seront perdues. Voulez-vous continuer ?")) {
                return;
            }
        }
        processingMode = mode;
        document.getElementById('blurModeButton').classList.toggle('selected', mode === 'blur');
        document.getElementById('emojiModeButton').classList.toggle('selected', mode === 'emoji');
        ImageProcessing.generateBlurredImage();
    },
    toggleCustomCursorDisplay(event, mode) {
        DOM.customCursor.style.display = drawingMode !== 'cursor' ? 'block' : 'none';
        Drawing.updateCursorPosition(event, mode);
    },
    resetToolButtons() {
        ['cursorButton', 'brushButton', 'eraserButton', 'emojiButton'].forEach(id =>
            document.getElementById(id).classList.remove('active')
        );
        document.getElementById('cursorButton').classList.add('active');
    },
    showLoadingOverlay() {
        DOM.loadingOverlay.style.display = 'flex';
    },
    hideLoadingOverlay() {
        DOM.loadingOverlay.style.display = 'none';
    }
};

// -----------------------------
// MODULE DRAWING
// -----------------------------
const Drawing = {
    setDrawingMode(mode) {
        drawingMode = mode;
        DOM.outputCanvas.style.cursor = mode === 'cursor' ? 'default' : 'none';
    },
    updateCursorPosition: function(event, mode) {
        const rect = DOM.outputCanvas.getBoundingClientRect();
        const scale = Math.min(rect.width / DOM.outputCanvas.width, rect.height / DOM.outputCanvas.height);
        const cursorSize = brushSize * 1.2 * scale;
        const clientX = event.clientX || event.pageX,
            clientY = event.clientY || event.pageY;

        if (mode === 'sliderView') {
            DOM.customCursor.style.left = `${window.scrollX + rect.left + rect.width / 2 - cursorSize / 2}px`;
            DOM.customCursor.style.top = `${window.scrollY + rect.top + 100 - cursorSize / 2}px`;
        } else {
            DOM.customCursor.style.left = `${clientX + window.scrollX - cursorSize / 2}px`;
            DOM.customCursor.style.top = `${clientY + window.scrollY - cursorSize / 2}px`;
        }

        if (drawingMode === 'emoji') {
            if (customEmojiImage) {
                DOM.customCursor.innerHTML = `<img src="${customEmojiImage.src}" style="width:100%; height:100%; object-fit:contain;" />`;
            } else {
                DOM.customCursor.innerHTML = selectedEmoji;
            }
            DOM.customCursor.style.fontSize = `${cursorSize}px`;
            DOM.customCursor.style.width = `${cursorSize}px`;
            DOM.customCursor.style.height = `${cursorSize}px`;
            DOM.customCursor.style.border = 'none';
            DOM.customCursor.style.borderRadius = 0;
            DOM.customCursor.style.background = "none";
        } else {
            if (drawingMode !== 'cursor') {
                DOM.customCursor.classList.add('customCursor');
                DOM.customCursor.innerHTML = '';
                DOM.customCursor.style.width = `${cursorSize}px`;
                DOM.customCursor.style.height = `${cursorSize}px`;
                DOM.customCursor.style.borderRadius = '50%';
                DOM.customCursor.style.border = `3px solid ${drawingMode === 'brush' ? 'blue' : 'red'}`;
                DOM.customCursor.style.backgroundColor = "rgba(255, 255, 255, 0.8)";
            } else {
                DOM.customCursor.style.display = 'none';
            }
        }
    },
    startDrawing(event) {
        isDrawing = true;
        this.draw(event);
    },
    stopDrawing() {
        isDrawing = false;
    },
    draw(event) {
        if (!isDrawing) return;
        isModified = true;
        const ctx = DOM.ctx;
        const {
            offsetX,
            offsetY
        } = Utils.getRelativePosition(event, DOM.outputCanvas);
        const radius = brushSize * 0.6;
        if (drawingMode === 'brush') {
            const tempCanvas = document.createElement('canvas');
            tempCanvas.width = radius * 2;
            tempCanvas.height = radius * 2;
            const tempCtx = tempCanvas.getContext('2d');
            tempCtx.drawImage(DOM.outputCanvas, offsetX - radius, offsetY - radius, radius * 2, radius * 2, 0, 0, radius * 2, radius * 2);
            const imageData = tempCtx.getImageData(0, 0, radius * 2, radius * 2);
            const blurredData = ImageProcessing.applyGaussianBlur(imageData, 5);
            tempCtx.putImageData(blurredData, 0, 0);
            ctx.save();
            ctx.beginPath();
            ctx.arc(offsetX, offsetY, radius, 0, 2 * Math.PI);
            ctx.clip();
            ctx.drawImage(tempCanvas, offsetX - radius, offsetY - radius);
            ctx.restore();
        } else if (drawingMode === 'eraser') {
            const scaleX = originalImage.width / DOM.outputCanvas.width,
                scaleY = originalImage.height / DOM.outputCanvas.height,
                originalX = Math.floor(offsetX * scaleX),
                originalY = Math.floor(offsetY * scaleY);
            const eraserData = ctx.getImageData(offsetX - radius, offsetY - radius, radius * 2, radius * 2);
            for (let y = 0; y < radius * 2; y++) {
                for (let x = 0; x < radius * 2; x++) {
                    const srcX = originalX - Math.floor(radius * scaleX) + Math.floor(x * scaleX),
                        srcY = originalY - Math.floor(radius * scaleY) + Math.floor(y * scaleY);
                    if (srcX >= 0 && srcX < originalImage.width && srcY >= 0 && srcY < originalImage.height) {
                        const srcIndex = (srcY * originalImage.width + srcX) * 4,
                            destIndex = (y * eraserData.width + x) * 4,
                            distance = Math.sqrt((x - radius) ** 2 + (y - radius) ** 2),
                            opacity = Math.max(0, Math.min(1, distance / radius));
                        for (let i = 0; i < 3; i++) {
                            eraserData.data[destIndex + i] =
                                opacity * eraserData.data[destIndex + i] +
                                (1 - opacity) * originalImageData.data[srcIndex + i];
                        }
                        eraserData.data[destIndex + 3] = eraserData.data[destIndex + 3];
                    }
                }
            }
            ctx.putImageData(eraserData, offsetX - radius, offsetY - radius);
        }
    },
    drawEmoji: function(ctx, x, y) {
        const cursorSize = brushSize * 1.2;
        if (customEmojiImage) {
            fabric.Image.fromURL(customEmojiImage.src, function(fabricImg) {
                fabricImg.set({
                    left: x,
                    top: y,
                    originX: 'center',
                    originY: 'center',
                    selectable: true,
                    hasControls: true
                });
                const scale = cursorSize / Math.max(customEmojiWidth, customEmojiHeight);
                fabricImg.scale(scale);
                fabricCanvas.add(fabricImg);
                fabricCanvas.renderAll();
            });

        } else {
            const emoji = new fabric.Text(selectedEmoji, {
                left: x,
                top: y + cursorSize * 0.2,
                fontSize: cursorSize,
                originX: 'center',
                originY: 'center',
                selectable: true,
                hasControls: true
            });
            fabricCanvas.add(emoji);
            fabricCanvas.renderAll();
        }
    }
};

// -----------------------------
// MODULE IMAGE PROCESSING
// -----------------------------
const ImageProcessing = {
    applyGaussianBlur(imageData, blurRadius) {
        const fxCanvas = fx.canvas(),
            texture = fxCanvas.texture(imageData);
        fxCanvas.draw(texture).triangleBlur(blurRadius).update();
        const tempCanvas = document.createElement('canvas');
        tempCanvas.width = imageData.width;
        tempCanvas.height = imageData.height;
        tempCanvas.getContext('2d').drawImage(fxCanvas, 0, 0);
        return tempCanvas.getContext('2d').getImageData(0, 0, tempCanvas.width, tempCanvas.height);
    },
    calculateBlurRadius(faceWidth) {
        return Math.round(Math.max(MIN_BLUR_RADIUS, Math.min(MAX_BLUR_RADIUS, (faceWidth / REFERENCE_FACE_WIDTH) * 50)));
    },
    generateBlurredImage() {
        UI.showLoadingOverlay();
        isModified = true;
        setTimeout(() => {
            const ctx = DOM.ctx;
            DOM.outputCanvas.width = originalImage.width;
            DOM.outputCanvas.height = originalImage.height;
            ctx.drawImage(originalImage, 0, 0);
            const tempCanvas = document.createElement('canvas');
            tempCanvas.width = DOM.outputCanvas.width;
            tempCanvas.height = DOM.outputCanvas.height;
            const tempCtx = tempCanvas.getContext('2d');
            sortedDetections.forEach((detection, index) => {
                if (faces[index]) {
                    const {
                        x,
                        y,
                        width,
                        height
                    } = detection.detection.box;
                    if (processingMode === 'blur') {
                        const centerX = x + width / 2,
                            centerY = y + height / 2 - height / 10,
                            scale = 1.2,
                            radius = (Math.max(width, height) / 2) * scale;
                        tempCtx.clearRect(0, 0, tempCanvas.width, tempCanvas.height);
                        tempCtx.drawImage(DOM.outputCanvas, centerX - radius, centerY - radius, radius * 2, radius * 2, centerX - radius, centerY - radius, radius * 2, radius * 2);
                        const imageData = tempCtx.getImageData(centerX - radius, centerY - radius, radius * 2, radius * 2),
                            blurredData = this.applyGaussianBlur(imageData, this.calculateBlurRadius(width));
                        tempCtx.putImageData(blurredData, centerX - radius, centerY - radius);
                        const gradient = tempCtx.createRadialGradient(centerX, centerY, 0, centerX, centerY, radius);
                        gradient.addColorStop(0, 'rgba(255,255,255,1)');
                        gradient.addColorStop(0.8, 'rgba(255,255,255,1)');
                        gradient.addColorStop(1, 'rgba(255,255,255,0)');
                        tempCtx.globalCompositeOperation = 'destination-in';
                        tempCtx.fillStyle = gradient;
                        tempCtx.beginPath();
                        tempCtx.arc(centerX, centerY, radius, 0, 2 * Math.PI);
                        tempCtx.fill();
                        ctx.save();
                        ctx.beginPath();
                        ctx.arc(centerX, centerY, radius, 0, 2 * Math.PI);
                        ctx.clip();
                        ctx.drawImage(tempCanvas, 0, 0);
                        ctx.restore();
                        tempCtx.globalCompositeOperation = 'source-over';
                        faceEmojis[index].set('visible', false);
                        fabricCanvas.renderAll();
                    } else if (processingMode === 'emoji') {
                        const custom = faceCustoms[index];
                        if (!custom) return;
                        faceEmojis[index].set('visible', true);
                        fabricCanvas.renderAll();
                        if (custom.type === 'image') {
                            const imgOriginalW = custom.value.width,
                                imgOriginalH = custom.value.height,
                                scale = 1.2,
                                boundingSize = Math.max(width, height) * scale,
                                imgRatio = imgOriginalW / imgOriginalH;
                            let drawWidth, drawHeight;
                            if (imgRatio >= 1) {
                                drawWidth = boundingSize;
                                drawHeight = boundingSize / imgRatio;
                            } else {
                                drawWidth = boundingSize * imgRatio;
                                drawHeight = boundingSize;
                            }
                            const leftEye = detection.landmarks.getLeftEye(),
                                rightEye = detection.landmarks.getRightEye(),
                                eyeAngle = Math.atan2(rightEye[0].y - leftEye[0].y, rightEye[0].x - leftEye[0].x);
                            ctx.save();
                            ctx.translate(x + width / 2, y + height / 2);
                            ctx.rotate(eyeAngle);
                            ctx.drawImage(custom.value, -drawWidth / 2, -drawHeight / 2, drawWidth, drawHeight);
                            ctx.restore();
                        }
                    }
                } else {
                    faceEmojis[index].set('visible', false);
                    fabricCanvas.renderAll();
                }
            });
            DOM.downloadButton.style.display = 'inline-block';
            UI.hideLoadingOverlay();
        }, 10);
    }
};

// -----------------------------
// MODULE HANDLERS
// -----------------------------
const Handlers = {
    handleImageUpload: async function(event) {
        if (isModified && !confirm("Attention : le travail sera perdu. Continuer ?")) {
            event.target.value = '';
            return;
        }
        UI.showLoadingOverlay();
        const file = event.target.files[0];
        window.filename = file.name.split('.').slice(0, -1).join('.');
        window.fileExtension = file.name.split('.').pop().toLowerCase();
        const img = new Image();
        DOM.selectAllButton.textContent = "Sélectionner tout";
        img.onload = async () => {
            let width = img.width;
            let height = img.height;

            const scaled = getScaledDimensions(width, height);
            width = scaled.width;
            height = scaled.height;


            faces = [];
            allSelected = false;
            document.getElementById("content").style.display = "block";

            const tempCanvas = document.createElement('canvas');
            tempCanvas.width = width;
            tempCanvas.height = height;
            const tempCtx = tempCanvas.getContext('2d');
            tempCtx.drawImage(img, 0, 0, width, height);

            originalImage = new Image();
            originalImage.src = tempCanvas.toDataURL();

            originalImage.onload = async () => {
                DOM.outputCanvas.width = width;
                DOM.outputCanvas.height = height;
                DOM.ctx.drawImage(originalImage, 0, 0);
                const newCornerSize = height * 20 / 800;
                fabric.Object.prototype.cornerSize = newCornerSize;
                originalImageData = DOM.ctx.getImageData(0, 0, DOM.outputCanvas.width, DOM.outputCanvas.height);
                originalWidth = img.width;
                originalHeight = img.height;
                aspectRatio = originalWidth / originalHeight;
                const rect = DOM.outputCanvas.getBoundingClientRect();
                scaleFactor = DOM.outputCanvas.width / rect.width;
                DOM.sizeSlider.value = 30;
                brushSize = parseInt(30 * originalWidth / 400, 10);
                DOM.widthInput.value = originalWidth;
                DOM.heightInput.value = originalHeight;
                syncCanvasSizes();
                const ssdOptions = new faceapi.SsdMobilenetv1Options({
                    minConfidence: 0.4,
                    maxResults: 200
                });
                faceDetections = await faceapi.detectAllFaces(DOM.outputCanvas, ssdOptions).withFaceLandmarks();
                uniqueDetections = Utils.filterDuplicateDetections(faceDetections, 0.1);
                sortedDetections = Utils.sortDetectionsByReadingOrder(uniqueDetections);
                const displaySize = {
                    width: DOM.outputCanvas.width,
                    height: DOM.outputCanvas.height
                };
                const resizedDetections = faceapi.resizeResults(sortedDetections, displaySize);
                DOM.facesContainer.innerHTML = '';
                ImageProcessing.generateBlurredImage();
                resizedDetections.forEach((detection, index) => {
                    const faceCanvas = document.createElement('canvas'),
                        faceContext = faceCanvas.getContext('2d'),
                        box = Utils.enlargeBox(detection.detection.box, 1.4, DOM.outputCanvas);
                    faceCanvas.width = 110;
                    faceCanvas.height = Math.floor(110 * box.height / box.width);
                    faceContext.drawImage(DOM.outputCanvas, box.x, box.y, box.width, box.height, 0, 0, faceCanvas.width, faceCanvas.height);
                    const faceBox = document.createElement('div');
                    faceBox.classList.add('faceBox');
                    faceBox.id = `face${index}`;
                    faceBox.appendChild(faceCanvas);
                    const chooseEmojiBtn = document.createElement('button');
                    chooseEmojiBtn.textContent = '😊';
                    chooseEmojiBtn.setAttribute('data-tooltip', "Modifier l'émoji");
                    chooseEmojiBtn.classList.add('choose-emoji-button', 'hidden');
                    chooseEmojiBtn.id = "chooseEmojiBtn-face-" + index;
                    chooseEmojiBtn.addEventListener('click', () => Handlers.openEmojiModal(index));
                    faceBox.appendChild(chooseEmojiBtn);
                    const resetBtn = document.createElement('button');
                    resetBtn.textContent = "◉";
                    resetBtn.setAttribute('data-tooltip', 'Réinitialiser la position');
                    resetBtn.classList.add('reset-position-button', 'hidden');
                    resetBtn.addEventListener('click', function(e) {
                        e.stopPropagation();
                        const element = faceEmojis[index];
                        if (element) {
                            element.set({
                                left: element.initialLeft,
                                top: element.initialTop,
                                angle: element.initialAngle || 0,
                                scaleX: element.initialScaleX,
                                scaleY: element.initialScaleY
                            });
                            fabricCanvas.renderAll();
                        }
                    });
                    faceBox.appendChild(resetBtn);


                    DOM.facesContainer.appendChild(faceBox);
                    faceCustoms[index] = {
                        type: 'emoji',
                        value: selectedEmoji
                    };
                    faces[index] = false;
                    faceBox.style.border = `10px solid green`;
                    faceCanvas.addEventListener('click', () => Handlers.toggleBlur(index, "canvas"));
                    const leftEye = detection.landmarks.getLeftEye(),
                        rightEye = detection.landmarks.getRightEye(),
                        eyeAngle = Math.atan2(rightEye[0].y - leftEye[0].y, rightEye[0].x - leftEye[0].x);
                    const emoji = new fabric.Text(faceCustoms[index].value, {
                        left: box.x + box.width / 2,
                        top: box.y + box.height / 2,
                        fontSize: Math.max(box.width, box.height) * 1.2,
                        originX: 'center',
                        originY: 'center',
                        selectable: true,
                        hasControls: true,
                        visible: false,
                        angle: eyeAngle * (180 / Math.PI)
                    });
                    emoji.initialLeft = box.x + box.width / 2;
                    emoji.initialTop = box.y + box.height / 2;
                    emoji.initialAngle = eyeAngle * (180 / Math.PI);
                    emoji.initialScaleX = emoji.scaleX;
                    emoji.initialScaleY = emoji.scaleY;
                    fabricCanvas.add(emoji);
                    faceEmojis[index] = emoji;
                });
                Drawing.setDrawingMode('cursor');
                UI.resetToolButtons();
                DOM.sizeSliderZone.style.display = 'none';
                UI.hideLoadingOverlay();
            };
        };
        img.src = URL.createObjectURL(file);
    },
    handlePointerDown(event) {
        event.preventDefault();
        DOM.customCursor.style.display = drawingMode !== "cursor" ? 'block' : 'none';
        Drawing.updateCursorPosition(event, '');
        if (drawingMode === 'emoji' && !isDrawingEmoji) {
            const {
                offsetX,
                offsetY
            } = Utils.getRelativePosition(event, DOM.outputCanvas);
            Drawing.drawEmoji(DOM.ctx, offsetX, offsetY);
            isDrawingEmoji = true;
            isModified = true;
        } else if (drawingMode !== 'emoji') {
            Drawing.startDrawing(event);
        }
    },
    handlePointerMove(event) {
        event.preventDefault();
        Drawing.updateCursorPosition(event, '');
        Drawing.draw(event);
    },
    handlePointerUp(event) {
        event.preventDefault();
        isDrawingEmoji = false;
        Drawing.stopDrawing();
    },
    handlePointerOut(event) {
        event.preventDefault();
        isDrawingEmoji = false;
        DOM.customCursor.style.display = 'none';
        Drawing.stopDrawing();
    },
    handleSizeSliderInput(event) {
        brushSize = parseInt(event.target.value * originalWidth / 400, 10);
        Drawing.updateCursorPosition(event, 'sliderView');
    },
    toggleSelectAll() {
        allSelected = !allSelected;
        for (let i = 0; i < faces.length; i++) {
            faces[i] = allSelected;
            const faceBox = document.getElementById(`face${i}`);
            faceBox.style.border = faces[i] ? `10px solid red` : `10px solid green`;
        }
        DOM.selectAllButton.textContent = allSelected ? 'Désélectionner tout' : 'Sélectionner tout';
        ImageProcessing.generateBlurredImage();
    },
    toggleBlur(index, mode) {
        if (mode === "canvas") {
            faces[index] = !faces[index];
            if (processingMode === "emoji") {
                fabricCanvas.discardActiveObject();
                faceEmojis[index].set('visible', faces[index]);
                faces[index] && fabricCanvas.setActiveObject(faceEmojis[index]);
                fabricCanvas.renderAll();
            } else {
                ImageProcessing.generateBlurredImage();
            }
        } else {
            faces[index] = true;
            if (processingMode === 'emoji') {
                if (faceEmojis[index]) {
                    fabricCanvas.remove(faceEmojis[index]);
                    delete faceEmojis[index];
                }
                if (faces[index]) {
                    const detection = sortedDetections[index],
                        box = detection.detection.box,
                        leftEye = detection.landmarks.getLeftEye(),
                        rightEye = detection.landmarks.getRightEye(),
                        eyeAngle = Math.atan2(rightEye[0].y - leftEye[0].y, rightEye[0].x - leftEye[0].x);
                    if (mode === "emojiCustomImage") {
                        fabric.Image.fromURL(faceCustoms[index].value.src, function(emoji) {
                            emoji.set({
                                left: box.x + box.width / 2,
                                top: box.y + box.height / 2,
                                originX: 'center',
                                originY: 'center',
                                selectable: true,
                                hasControls: true,
                                angle: eyeAngle * (180 / Math.PI)
                            });
                            emoji.initialLeft = box.x + box.width / 2;
                            emoji.initialTop = box.y + box.height / 2;
                            emoji.initialAngle = eyeAngle * (180 / Math.PI);

                            const scale = Math.max(box.width, box.height) / Math.max(emoji.width, emoji.height);
                            emoji.scale(scale);
                            emoji.initialScaleX = emoji.scaleX;
                            emoji.initialScaleY = emoji.scaleY;

                            fabricCanvas.add(emoji);
                            faceEmojis[index] = emoji;
                            fabricCanvas.setActiveObject(faceEmojis[index]);
                            fabricCanvas.renderAll();
                        });
                    } else {
                        const emoji = new fabric.Text(faceCustoms[index].value, {
                            left: box.x + box.width / 2,
                            top: box.y + box.height / 2,
                            fontSize: Math.max(box.width, box.height) * 1.2,
                            originX: 'center',
                            originY: 'center',
                            selectable: true,
                            hasControls: true,
                            angle: eyeAngle * (180 / Math.PI)
                        });
                        emoji.initialLeft = box.x + box.width / 2;
                        emoji.initialTop = box.y + box.height / 2;
                        emoji.initialAngle = eyeAngle * (180 / Math.PI);
                        emoji.initialScaleX = emoji.scaleX;
                        emoji.initialScaleY = emoji.scaleY;
                        fabricCanvas.add(emoji);
                        faceEmojis[index] = emoji;
                        fabricCanvas.setActiveObject(faceEmojis[index]);
                        fabricCanvas.renderAll();
                    }
                }
            }
        }
        const faceDiv = document.getElementById(`face${index}`);
        faceDiv.style.border = faces[index] ? `10px solid red` : `10px solid green`;
    },
    downloadImage() {
        const newWidth = parseInt(DOM.widthInput.value, 10),
            newHeight = parseInt(DOM.heightInput.value, 10);
        if (isNaN(newWidth) || isNaN(newHeight)) {
            alert("Veuillez entrer des dimensions valides.");
            return;
        }
        fabricCanvas.discardActiveObject();
        fabricCanvas.renderAll();
        const tempCanvas = document.createElement('canvas');
        tempCanvas.width = newWidth;
        tempCanvas.height = newHeight;
        const tempCtx = tempCanvas.getContext('2d');
        tempCtx.drawImage(DOM.outputCanvas, 0, 0, DOM.outputCanvas.width, DOM.outputCanvas.height, 0, 0, newWidth, newHeight);
        tempCtx.drawImage(fabricCanvas.lowerCanvasEl, 0, 0, fabricCanvas.lowerCanvasEl.width, fabricCanvas.lowerCanvasEl.height, 0, 0, newWidth, newHeight);
        let mimeType = 'image/jpeg',
            outExtension = 'jpg';
        if (window.fileExtension === 'webp') {
            mimeType = 'image/webp';
            outExtension = 'webp';
        }
        if (window.fileExtension === 'png') {
            mimeType = 'image/png';
            outExtension = 'png';
        }
        tempCanvas.toBlob(blob => {
            const url = URL.createObjectURL(blob);
            const link = document.createElement('a');
            link.href = url;
            link.download = `${window.filename}_blur.${outExtension}`;
            if (/Mobi|Android/i.test(navigator.userAgent)) {
                const newWindow = window.open(url, '_blank');
                if (!newWindow) alert("Veuillez autoriser les pop-ups pour afficher l'image.");
            } else {
                link.click();
            }
            URL.revokeObjectURL(url);
        }, mimeType);
    },
    openEmojiModal(faceIndex) {
        Handlers.currentFaceIndex = faceIndex;
        document.getElementById('emojiModal').style.display = 'block';
    },
    closeEmojiModal() {
        document.getElementById('emojiModal').style.display = 'none';
    },
    closeEmojiModalTool() {
        document.getElementById('emojiModalTool').style.display = 'none';
    }
};

// -----------------------------
// FONCTION DE RESIZE DU CANVAS
// -----------------------------
const syncCanvasSizes = () => {
    const container = document.getElementById('canvasContainer'),
        output = DOM.outputCanvas;
    container.style.height = 'auto';
    fabricCanvas.wrapperEl.style.height = 'auto';
    const newHeight = output.offsetHeight;
    fabricCanvas.setDimensions({
        width: output.width,
        height: output.height
    }, {
        cssOnly: false,
        backstoreOnly: false
    });
    fabricCanvas.calcViewportBoundaries();
    fabricCanvas.renderAll();
    container.style.height = `${newHeight}px`;
    fabricCanvas.wrapperEl.style.height = `${newHeight}px`;
};

// -----------------------------
// COLLAPSIBLES
// -----------------------------
const setupCollapsibles = () => {
    document.querySelectorAll(".collapsible").forEach(item => {
        item.addEventListener("click", function() {
            this.classList.toggle("active");
            const content = this.nextElementSibling;
            content.style.display = (content.style.display === "grid" || content.style.display === "block") ? "none" : "grid";
        });
    });
};

// -----------------------------
// MODULE APP
// -----------------------------
const App = {
    init: async function() {
        fabricCanvas = new fabric.Canvas('fabricCanvas', {
            selection: true,
            backgroundColor: 'transparent',
            hoverCursor: 'default'
        });
        Object.assign(fabric.Object.prototype, {
            cornerColor: 'white',
            cornerStrokeColor: '#025753',
            cornerSize: 15,
            cornerStyle: 'circle',
            borderColor: '#025753',
            transparentCorners: false
        });

        fabric.Object.prototype.controls.ml.visible = false;
        fabric.Object.prototype.controls.mt.visible = false;
        fabric.Object.prototype.controls.mr.visible = false;
        fabric.Object.prototype.controls.mb.visible = false;

        fabricCanvas.upperCanvasEl.id = "upperCanvas";
        new ResizeObserver(syncCanvasSizes).observe(DOM.outputCanvas);
        await this.loadModels();
        this.setupEventListeners();
        setupCollapsibles();
    },
    loadModels: async function() {
        await faceapi.nets.ssdMobilenetv1.loadFromUri('./weights/');
        await faceapi.nets.faceLandmark68Net.loadFromUri('./weights/');
    },
    setupEventListeners: function() {
        DOM.imageInput.addEventListener('change', Handlers.handleImageUpload);
        DOM.downloadButton.addEventListener('click', Handlers.downloadImage);
        DOM.selectAllButton.addEventListener('click', Handlers.toggleSelectAll);
        DOM.widthInput.addEventListener('input', () => Utils.updateDimensionProportionally('width'));
        DOM.heightInput.addEventListener('input', () => Utils.updateDimensionProportionally('height'));
        DOM.sizeSlider.addEventListener('input', Handlers.handleSizeSliderInput);
        DOM.sizeSlider.addEventListener('mouseenter', e => UI.toggleCustomCursorDisplay(e, 'sliderView'));
        DOM.sizeSlider.addEventListener('touchstart', e => UI.toggleCustomCursorDisplay(e, 'sliderView'));
        const fcanvas = document.getElementById('upperCanvas');
        fcanvas.addEventListener('mouseenter', e => {
            UI.toggleCustomCursorDisplay(e, '');
        });
        fcanvas.addEventListener('mouseleave', () => {
            DOM.customCursor.style.display = 'none';
        });
        ['pointerdown', 'pointermove', 'pointerup', 'pointerout'].forEach(evt => {
            fcanvas.addEventListener(evt, e => {
                if (drawingMode === 'cursor') return;
                e.stopPropagation();
                if (evt === 'pointerdown') Handlers.handlePointerDown(e);
                else if (evt === 'pointermove') Handlers.handlePointerMove(e);
                else if (evt === 'pointerup') Handlers.handlePointerUp(e);
                else if (evt === 'pointerout') Handlers.handlePointerOut(e);
            });
        });
        const toolButtons = [{
                id: 'cursorButton',
                mode: 'cursor',
                sizeDisplay: 'none',
                label: ''
            },
            {
                id: 'brushButton',
                mode: 'brush',
                sizeDisplay: 'flex',
                label: 'Taille du pinceau'
            },
            {
                id: 'eraserButton',
                mode: 'eraser',
                sizeDisplay: 'flex',
                label: 'Taille de la gomme'
            },
            {
                id: 'emojiButton',
                mode: 'emoji',
                sizeDisplay: 'flex',
                label: 'Taille de l\'émoji'
            },
        ];
        toolButtons.forEach(({
            id,
            mode,
            sizeDisplay,
            label
        }) => {
            document.getElementById(id).addEventListener('click', e => {
                UI.setActiveButton(id, mode, sizeDisplay, label);
                Drawing.updateCursorPosition(e, 'sliderView');
            });
        });
        document.getElementById('blurModeButton').addEventListener('click', () => {
            UI.setProcessingMode('blur');
            document.querySelectorAll('.choose-emoji-button').forEach(btn => btn.classList.add('hidden'));
            document.querySelectorAll('.reset-position-button').forEach(btn => btn.classList.remove('hidden'));
        });
        document.getElementById('emojiModeButton').addEventListener('click', () => {
            UI.setProcessingMode('emoji');
            document.querySelectorAll('.choose-emoji-button').forEach(btn => btn.classList.remove('hidden'));
            document.querySelectorAll('.reset-position-button').forEach(btn => btn.classList.remove('hidden'));
        });
        document.getElementById('emojiBtn1').addEventListener('click', () => {
            document.getElementById('emojiPanel1').style.display = "flex";
            document.getElementById('emojiPanel2').style.display = "none";
        });
        document.getElementById('emojiBtn2').addEventListener('click', () => {
            document.getElementById('emojiPanel1').style.display = "none";
            document.getElementById('emojiPanel2').style.display = "flex";
        });
        document.getElementById('emojiBtnTool1').addEventListener('click', () => {
            document.getElementById('emojiPanelTool1').style.display = "flex";
            document.getElementById('emojiPanelTool2').style.display = "none";
        });
        document.getElementById('emojiBtnTool2').addEventListener('click', () => {
            document.getElementById('emojiPanelTool1').style.display = "none";
            document.getElementById('emojiPanelTool2').style.display = "flex";
        });
        document.getElementById('emojiModalClose').addEventListener('click', Handlers.closeEmojiModal);
        document.getElementById('emojiModalToolClose').addEventListener('click', Handlers.closeEmojiModalTool);
        document.getElementById('selectEmojiButton').addEventListener('click', () => {
            document.getElementById('emojiModalTool').style.display = 'block';
        });
        document.querySelectorAll('#emojiList .emoji-item').forEach(btn => {
            btn.addEventListener('click', function() {
                const chosenEmoji = this.textContent;
                const faceIndex = Handlers.currentFaceIndex;
                faceCustoms[faceIndex] = {
                    type: 'emoji',
                    value: chosenEmoji
                };
                document.getElementById('chooseEmojiBtn-face-' + faceIndex).textContent = chosenEmoji;
                selectedEmoji = chosenEmoji;
                document.getElementById('selectEmojiButton').textContent = chosenEmoji;
                Handlers.closeEmojiModal();
                Handlers.toggleBlur(faceIndex, "emojiCustom");
            });
        });
        document.querySelectorAll('#emojiListTool .emoji-item').forEach(btn => {
            btn.addEventListener('click', function() {
                const chosenEmoji = this.textContent;
                selectedEmoji = chosenEmoji;
                customEmojiImage = null;
                document.getElementById('selectEmojiButton').textContent = chosenEmoji;
                Handlers.closeEmojiModalTool();
            });
        });
        document.getElementById('customEmojiInput').addEventListener('change', async function(e) {
            const file = e.target.files[0];
            if (!file) return;
            const reader = new FileReader();
            reader.onload = function(ev) {
                const dataUrl = ev.target.result;
                const img = new Image();
                img.onload = function() {
                    const faceIndex = Handlers.currentFaceIndex;
                    faceCustoms[faceIndex] = {
                        type: 'image',
                        value: img
                    };
                    document.getElementById('chooseEmojiBtn-face-' + faceIndex).innerHTML =
                        `<img src="${dataUrl}" style="width:40px; height:40px; object-fit:contain;" alt="miniature" />`;
                    Handlers.closeEmojiModal();
                    Handlers.toggleBlur(faceIndex, "emojiCustomImage");
                };
                img.src = dataUrl;
            };
            reader.readAsDataURL(file);
        });


        document.getElementById('customImageInput').addEventListener('change', function(e) {
            const file = e.target.files[0];
            if (!file) return;

            const reader = new FileReader();
            reader.onload = function(ev) {
                const dataUrl = ev.target.result;
                const img = new Image();
                img.onload = function() {
                    customEmojiImage = img;
                    customEmojiWidth = img.naturalWidth || img.width;
                    customEmojiHeight = img.naturalHeight || img.height;

                    const cursorElement = document.getElementById('customCursor');
                    cursorElement.innerHTML = `<img src="${dataUrl}" style="width:100%; height:100%; object-fit:cover;" />`;
                    cursorElement.style.display = 'block';

                    drawingMode = 'emoji';
                    Handlers.closeEmojiModalTool();
                };
                img.src = dataUrl;
            };
            reader.readAsDataURL(file);
        });

        document.addEventListener("keydown", function(e) {
            if (e.key === "Delete" || e.key === "Del" || e.keyCode === 46) {
                const activeObjects = fabricCanvas.getActiveObjects();
                if (activeObjects.length) {
                    activeObjects.forEach(obj => {
                        const index = faceEmojis.indexOf(obj);
                        if (index !== -1) {
                            const faceDiv = document.getElementById("face" + index);
                            if (faceDiv) faceDiv.style.border = "10px solid green";
                            faceEmojis[index].set('visible', false);
                            faces[index] = false;
                        } else {
                            fabricCanvas.remove(obj);
                        }
                    });
                    fabricCanvas.discardActiveObject();
                    fabricCanvas.renderAll();
                }
            }
        });

        fabricCanvas.on('mouse:dblclick', function(e) {
            const target = e.target;
            if (target) {
                const index = faceEmojis.indexOf(target);
                if (index !== -1) {
                    Handlers.openEmojiModal(index);
                }
            }
        });

        let lastTapTime = 0;
        let longPressTimer = null;
        let longPressTarget = null;
        let startX = 0,
            startY = 0;
        const LONG_PRESS_DURATION = 1000;
        const DOUBLE_TAP_THRESHOLD = 300;
        const MOVE_THRESHOLD = 10;

        function handleTouchStart(e) {
            if (e.touches && e.touches.length === 1) {
                const touch = e.touches[0];
                startX = touch.pageX;
                startY = touch.pageY;
                longPressTriggered = false;
                longPressTimer = setTimeout(() => {
                    longPressTriggered = true;
                    const target = fabricCanvas.findTarget(e);
                    if (target) {
                        showContextMenu(startX, startY);
                    }
                }, LONG_PRESS_DURATION);
            }
        }

        function handleTouchMove(e) {
            if (e.touches && e.touches.length === 1) {
                const touch = e.touches[0];
                const deltaX = Math.abs(touch.pageX - startX);
                const deltaY = Math.abs(touch.pageY - startY);
                if (deltaX > MOVE_THRESHOLD || deltaY > MOVE_THRESHOLD) {
                    clearTimeout(longPressTimer);
                    longPressTimer = null;
                }
            }
        }

        function handleTouchEnd(e) {
            clearTimeout(longPressTimer);
            if (!longPressTriggered) {
                const currentTime = Date.now();
                const tapInterval = currentTime - lastTapTime;
                if (tapInterval > 0 && tapInterval < DOUBLE_TAP_THRESHOLD) {
                    const target = fabricCanvas.findTarget(e);
                    if (target) {
                        const index = faceEmojis.indexOf(target);
                        if (index !== -1) {
                            Handlers.openEmojiModal(index);
                        }
                    }
                    e.preventDefault();
                }
                lastTapTime = currentTime;
            }
        }

        function showContextMenu(x, y) {
            const menu = document.getElementById('contextMenu');
            if (menu) {
                menu.style.display = 'block';
                menu.style.left = x + 'px';
                menu.style.top = y + 'px';
            }
        }

        function hideContextMenu() {
            const menu = document.getElementById('contextMenu');
            if (menu) {
                menu.style.display = 'none';
            }
        }

        fabricCanvas.upperCanvasEl.addEventListener('touchstart', handleTouchStart, false);
        fabricCanvas.upperCanvasEl.addEventListener('touchmove', handleTouchMove, false);
        fabricCanvas.upperCanvasEl.addEventListener('touchend', handleTouchEnd, false);


        document.getElementById('deleteOption').addEventListener('click', function() {
            const activeObjects = fabricCanvas.getActiveObjects();
            if (activeObjects.length) {
                activeObjects.forEach(obj => {
                    const index = faceEmojis.indexOf(obj);
                    if (index !== -1) {
                        const faceDiv = document.getElementById("face" + index);
                        if (faceDiv) {
                            faceDiv.style.border = "10px solid green";
                        }
                        faceEmojis[index].set('visible', false);
                        faces[index] = false;
                    } else {
                        fabricCanvas.remove(obj);
                    }
                });
                fabricCanvas.discardActiveObject();
                fabricCanvas.renderAll();
            }
            hideContextMenu();
        });

        fabricCanvas.on('selection:created', function(e) {
            const selectedItems = e.selected || [];
            updateBlinkForSelection(selectedItems);
            hideContextMenu();
        });
        fabricCanvas.on('selection:updated', function(e) {
            const selectedItems = e.selected || [];
            updateBlinkForSelection(selectedItems);
            hideContextMenu();
        });
        fabricCanvas.on('selection:cleared', function() {
            document.querySelectorAll('.faceBox').forEach(faceDiv => faceDiv.classList.remove('blink-border'));
            hideContextMenu();
        });

        function updateBlinkForSelection(selectedItems) {
            document.querySelectorAll('.faceBox').forEach(faceDiv => faceDiv.classList.remove('blink-border'));
            selectedItems.forEach(item => {
                const index = faceEmojis.indexOf(item);
                if (index !== -1) {
                    const faceDiv = document.getElementById("face" + index);
                    if (faceDiv) {
                        faceDiv.classList.add("blink-border");
                    }
                }
            });
        }

        const accordions = document.querySelectorAll(".accordion");
        accordions.forEach(accordion => {
            accordion.addEventListener("click", function() {
                this.classList.toggle("active");
                const panel = this.nextElementSibling;
                if (panel.style.maxHeight) {
                    panel.style.maxHeight = null;
                } else {
                    panel.style.maxHeight = panel.scrollHeight + "px";
                }
            });
        });
    }
};

// -----------------------------
// INITIALISATION DE L'APPLICATION
// -----------------------------
document.addEventListener('DOMContentLoaded', () => {
    App.init();
});